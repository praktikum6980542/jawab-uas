#include <iostream>
using namespace std;

// Deklarasi fungsi dan prosedur
double hitungNilai(double absen, double tugas, double quiz, double uts, double uas);
char hitungHurufMutu(double nilai);

int main() {
    	double nilai, quiz, absen, uts, uas, tugas;
        	char Huruf_Mutu;

            	// Inisialisasi nilai
                	quiz = 40; absen = 100; uts = 60; uas = 50; tugas = 80;

                    	cout << "Absen = " << absen << " UTS = " << uts << endl;
                        	cout << "Tugas = " << tugas << " UAS = " << uas << endl;
                            	cout << "Quiz = " << quiz << endl;

                                	// Memanggil fungsi hitungNilai
                                    	nilai = hitungNilai(absen, tugas, quiz, uts, uas);

                                        	// Memanggil fungsi hitungHurufMutu
                                            	Huruf_Mutu = hitungHurufMutu(nilai);

                                                	cout << "Huruf Mutu : " << Huruf_Mutu << endl;

                                                    	return 0;
}

// Definisi fungsi hitungNilai
double hitungNilai(double absen, double tugas, double quiz, double uts, double uas) {
    	return ((0.1 * absen) + (0.2 * tugas) + (0.3 * quiz) + (0.4 * uts) + (0.5 * uas)) / 2;
}

// Definisi fungsi hitungHurufMutu
char hitungHurufMutu(double nilai) {
    	char Huruf_Mutu;

        	if (nilai > 85 && nilai <= 100)
                	Huruf_Mutu = 'A';
                    	else if (nilai > 70 && nilai <= 85)
                            	Huruf_Mutu = 'B';
                                	else if (nilai > 55 && nilai <= 70)
                                        	Huruf_Mutu = 'C';
                                            	else if (nilai > 40 && nilai <= 55)
                                                    	Huruf_Mutu = 'D';
                                                        	else if (nilai >= 0 && nilai <= 40)
                                                                	Huruf_Mutu = 'E';

                                                                    	return Huruf_Mutu;
}
}
}
}
